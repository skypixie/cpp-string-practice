﻿#include <iostream>
#include <string>

int main()
{
	std::string string{ "Skillbox" };
	
	// используем тип size_t, тк компилятор жалуется на возможную потерю данных
	std::size_t stringLength{ string.length() };


	std::cout << string << '\n';
	std::cout << "Length: " << string.length() << '\n';

	// если строка будет пустая,
	// то мы не сможем получать символы по индексам
	if (stringLength)
	{
		std::cout << "First character: " << string[0] << '\n';
		std::cout << "Last character:  " << string[string.length() - 1] << '\n';
	}
}